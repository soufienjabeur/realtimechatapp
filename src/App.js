import React from 'react'
import { ChatEngine } from 'react-chat-engine';
import "./App.css";
import ChatFeed from "./components/ChatFeed";

const App = () => {
    return (
        <div>
            <ChatEngine
                height="100vh"
                projectID='223ddd7b-f6e4-42fd-a241-3d7a25abfd86'
                userName='Jabeur'
                userSecret='123123'
                renderChatFeed={(chatAppProps) => <ChatFeed{...chatAppProps} />}

            />
        </div>
    )
}

export default App
